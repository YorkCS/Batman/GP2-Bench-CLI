from tqdm import tqdm


class Progress(object):

    def __init__(self, name_0, name_1):
        self._phase = None
        self._prog = None
        self._pbar = None
        self._name_0 = name_0
        self._name_1 = name_1

    def update(self, phase, total, complete):
        if self._pbar is None:
            self._phase = 0
            self._prog = 0
            self._pbar = tqdm(total=total, smoothing=0, unit=' {}'.format(self._name_0), ncols=80, ascii=False)
        elif phase != self._phase:
            self._phase = 1
            self._prog = 0
            self._pbar.close()
            self._pbar = tqdm(total=total, smoothing=0, unit=' {}'.format(self._name_1), ncols=80, ascii=False)
        else:
            self._pbar.update(complete - self._prog)
            self._prog = complete

    def close(self):
        if self._pbar is not None:
            self._pbar.close()
            self._pbar = None
