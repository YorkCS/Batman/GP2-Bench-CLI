from grpc import insecure_channel

from bench_pb2 import *
from bench_pb2_grpc import *


class BenchHandler(object):

    def __init__(self, client):
        self._client = client

    def handle(self, programs, config, runs):
        progs = []

        for name, content in programs.items():
            progs.append(Program(name=name, content=content))

        for step in self._client.Execute(Setup(programs=progs, config=config, runs=runs)):
            yield step.progress if step.HasField('progress') else None, step.benchmarks if step.HasField('benchmarks') else None, step.error if step.HasField('error') else None


class BenchFactory(object):

    @staticmethod
    def make(host, port = None):
        MB = 1024 * 1024
        GRPC_CHANNEL_OPTIONS = [('grpc.max_message_length', 256 * MB), ('grpc.max_receive_message_length', 256 * MB)]

        return BenchHandler(
            BenchStub(insecure_channel('{}:{}'.format(host, port or 9111), options=GRPC_CHANNEL_OPTIONS))
        )
