from bench import BenchFactory
from progress import Progress
from tabulate import tabulate
import sys
import os

from config_pb2 import *


class CLIBench(object):

    def __init__(self, bench):
        self._bench = bench

    def run(self, progs, config, runs):
        prog = Progress('graphs', 'runs')
        try:
            self._handle(self._bench.handle(progs, config, runs), prog)
        finally:
            prog.close()

    def _handle(self, iter, prog):
        for progress, benchmarks, error in iter:
            if progress is not None:
                prog.update(progress.phase, progress.total, progress.complete)
            elif benchmarks is not None:
                prog.close()
                print("\n" + "Results for {}:".format(benchmarks.program));
                print("\n" + tabulate(self._average(benchmarks.results), headers=['Graph', 'Nodes', 'Edges', 'Success', 'Time'], tablefmt='github'), flush=True)
            elif error is not None:
                prog.close()
                print_error(error)

    def _average(self, results):
        for index, result in enumerate(results):
            yield '{}'.format(index + 1), '{}'.format(result.nodes), '{}'.format(result.edges), '{0:.2f}%'.format(result.rate * 100), '{0:.2f}ms'.format(result.time)


def print_error(msg):
    print("\033[1;31mError: " + msg + "\033[0;0m", file=sys.stderr, flush=True)
    sys.exit(2)


def main(args):
    bench = BenchFactory.make(os.environ['BENCH_HOST'])

    progs = {}

    for prog in args[2:]:
        try:
            with open(args[1] + '/' + prog, 'r') as f:
                progs[prog] = f.read()
        except Exception as e:
            print_error(str(e))

    range = Range(step=int(os.environ.get('GEN_STEP', '1')), min=int(os.environ.get('GEN_MIN', '1')), max=int(os.environ.get('GEN_MAX', '10')))
    config = Config(mode=os.environ.get('GEN_MODE', 'fsa'), range=range)

    CLIBench(bench).run(progs, config, int(os.environ.get('BENCH_RUNS', '20')))


if __name__ == '__main__':
    main(sys.argv)
