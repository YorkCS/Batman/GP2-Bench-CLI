# GP2 Bench CLI

## Prereqs

Any modern Linux or Mac running Docker 2018 CE or greater will suffice.

## Running

You can start the GP2 Bench CLI Docker instance, by providing it with the hostname or IP address of a GP2 Bench server:

```bash
$ docker run -t -v ${PWD}:/data -e BENCH_HOST='172.17.0.3' registry.gitlab.com/yorkcs/batman/gp2-bench-cli
```

It's possible to change the number of runs per graph per program with `-e BENCH_RUNS='100'`, where `100` is the desired number of runs. The default is `20` runs. The 4 generation params can be set by the `GEN_` variables.

## Building

In standard Docker fashion:

```bash
$ docker build -t registry.gitlab.com/yorkcs/batman/gp2-bench-cli:latest .
```

And then to publish:

```bash
$ docker push registry.gitlab.com/yorkcs/batman/gp2-bench-cli
```
